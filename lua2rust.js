/*
    lua2rust
    Copyright (C) 2019  Zaoqi <zaomir@outlook.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
Object.defineProperty(exports, "__esModule", { value: true });
const lua = require("luaparse");
const assert = require("assert");
function object_shadow_copy(x) {
    return Object.assign({}, x);
}
function pre(x) {
    return lua.parse(x, { comments: false, scope: false, locations: true });
}
exports.pre = pre;
function scope_copy0(x) {
    return { local: new Set(x.local), used_nonlocal: x.used_nonlocal };
}
function scope_new_closure() {
    return { local: new Set(), used_nonlocal: new Set() };
}
exports.scope_new_closure = scope_new_closure;
function scope_use(scope, x) {
    if (!scope.local.has(x)) {
        scope.used_nonlocal.add(x);
    }
}
const lua_nil = "_lua_nil()";
const lua_VarargLiteral_id = "_lua_tmp_vararg";
const lua_true = "_lua_true()";
const lua_false = "_lua_false()";
function lua_make_var(x) {
    return `std::sync::Arc::new(std::sync::RwLock::new(${x}))`;
}
function lua_get_var(x) {
    return `(${x}.read().unwrap().clone())`;
}
function lua_set_var(x, y) {
    return `*${x}.write().unwrap() = { let _lua_tmp = ${y}; _lua_tmp.clone() };\n`;
}
function lua_lookup(map, key, loc) {
    return `_lua_lookup(${map},${key}, ${loc})`;
}
function lua_set(map, key, val, loc) {
    return `_lua_set(${map},${key},${val}, ${loc});\n`;
}
function lua_table(map) {
    if (map.length === 0) {
        return `_lua_table(vec![])`;
    }
    return `_lua_table(vec![${map.map(x => `(${x[0]}, ${x[1]})`).reduce((x, y) => x + ", " + y)}])`;
}
function lua_str(x) {
    return `_lua_str(${JSON.stringify(x)})`;
}
function lua_num(x) {
    return `_lua_num!(${x.toString()})`;
}
function lua_call(x, args, loc) {
    if (args.length === 0) {
        return `_lua_call(${x}, vec![], ${loc})`;
    }
    return `_lua_call(${x}, vec![${args.reduce((x, y) => x + ", " + y)}], ${loc})`;
}
function lua_vec(vec) {
    return `_lua_vec(${vec})`;
}
function lua_lambda(arg_id, body, inner_scope) {
    const init = Array.from(inner_scope.used_nonlocal).map(x => `let ${x} = ${x}.clone();\n`).reduce(((x, y) => x + y), "");
    return `_lua_lambda(Box::new({${init}move |mut ${arg_id}, _| {\n${body}\n${lua_nil}\n}}))`; // TODO 检查此行是否正确
}
function lua_len(x, loc) {
    return `_lua_len(${x}, ${loc})`;
}
function lua_not(x, loc) {
    return `_lua_not(${x}, ${loc})`;
}
function lua_neg(x, loc) {
    return `_lua_neg(${x}, ${loc})`;
}
function lua_loc(x) {
    return `_lua_debug_loc { start: (${x.loc.start.line}, ${x.loc.start.column}), end: (${x.loc.end.line}, ${x.loc.end.column}) }`;
}
function lua_get_loc(x) {
    return x.loc;
}
function lua_op(op, x, y, loc) {
    const ops = {
        "+": "add",
        "-": "sub",
        "*": "mul",
        "/": "div",
        "%": "rem",
        "^": "exp",
        "==": "eq",
        "~=": "not_eq",
        "<=": "less_eq",
        ">=": "greater_eq",
        "<": "less",
        ">": "greater",
        "..": "concat",
        "and": "and",
        "or": "or",
    };
    return `_lua_op!{${ops[op]}, ${x}, ${y}, ${loc}}`;
}
const table = {
    Chunk: (x, scope) => x.body.length === 0 ? "" : x.body.map(x => do_table(x, scope)).reduce((x, y) => x + y),
    LabelStatement: (x) => { throw 'LabelStatement not supported'; },
    BreakStatement: (x) => 'break;\n',
    GotoStatement: (x) => { throw 'GotoStatement not supported'; },
    ReturnStatement: (x, scope) => {
        if (x.arguments.length === 0) {
            return `return ${lua_nil};\n`;
        }
        assert(x.arguments.length === 1, `multiple values returning not supported`);
        return `return ${do_table(x.arguments[0], scope)};\n`;
    },
    IfStatement: do_IfStatement,
    WhileStatement: (x, scope) => `while (${do_table(x.condition, scope)}).as_bool(${lua_loc(x)}) {\n${do_table_map(x.body, scope_copy0(scope))}}\n`,
    DoStatement: (x, scope) => `{\n${do_table_map(x.body, scope_copy0(scope))}}\n`,
    RepeatStatement: (x, scope) => {
        const s = scope_copy0(scope);
        return `loop {\n${do_table_map(x.body, s)}if (${do_table(x.condition, s)}).as_bool(${lua_loc(x)}) { break; }\n}\n`;
    },
    LocalStatement: do_LocalStatement,
    AssignmentStatement: do_AssignmentStatement,
    CallStatement: (x, scope) => `${do_table(x.expression, scope)};\n`,
    FunctionDeclaration: do_FunctionDeclaration,
    ForNumericStatement: do_ForNumericStatement,
    ForGenericStatement: do_ForGenericStatement,
    Identifier: (x, scope) => {
        scope_use(scope, x.name);
        // 以下是为了`i=i+1`之类的东西。
        return lua_get_var(x.name);
    },
    StringLiteral: (x) => lua_str(x.value),
    NumericLiteral: (x) => lua_num(x.value),
    BooleanLiteral: (x) => x.value ? lua_true : lua_false,
    NilLiteral: () => lua_nil,
    TableConstructorExpression: do_TableConstructorExpression,
    UnaryExpression: (x, scope) => {
        let op = x.operator;
        if (op === '#') {
            return lua_len(do_table(x.argument, scope), lua_loc(x));
        }
        else if (op === '-') {
            return lua_neg(do_table(x.argument, scope), lua_loc(x));
        }
        else {
            const _not = op;
            assert(_not === 'not');
            return lua_not(do_table(x.argument, scope), lua_loc(x));
        }
    },
    BinaryExpression: (x, scope) => lua_op(x.operator, do_table(x.left, scope), do_table(x.right, scope), lua_loc(x)),
    LogicalExpression: (x, scope) => lua_op(x.operator, do_table(x.left, scope), do_table(x.right, scope), lua_loc(x)),
    MemberExpression: (x, scope) => {
        assert(x.indexer === '.');
        return lua_lookup(do_table(x.base, scope), lua_str(x.identifier.name), lua_loc(x));
    },
    IndexExpression: (x, scope) => lua_lookup(do_table(x.base, scope), do_table(x.index, scope), lua_loc(x)),
    CallExpression: (x, scope) => lua_call(do_table(x.base, scope), x.arguments.map(x => do_table(x, scope)), lua_loc(x)),
    TableCallExpression: (x, scope) => lua_call(do_table(x.base, scope), [do_table(x.arguments, scope)], lua_loc(x)),
    StringCallExpression: (x, scope) => lua_call(do_table(x.base, scope), [do_table(x.argument, scope)], lua_loc(x)),
};
function do_AssignmentStatement(x, scope) {
    assert(x.init.length === 1 && x.variables.length === 1);
    const [vara, init] = [x.variables[0], x.init[0]];
    if (vara.type === 'Identifier') {
        scope_use(scope, vara.name);
        return lua_set_var(vara.name, do_table(init, scope));
    }
    else if (vara.type === 'MemberExpression') {
        assert(vara.indexer === '.');
        return lua_set(do_table(vara.base, scope), lua_str(vara.identifier.name), do_table(init, scope), lua_loc(x));
    }
    else {
        const _i = vara.type;
        assert(_i === 'IndexExpression');
        return lua_set(do_table(vara.base, scope), do_table(vara.index, scope), do_table(init, scope), lua_loc(x));
    }
}
function do_TableConstructorExpression(x, scope) {
    if (x.fields.length === 1 && x.fields[0].type === 'TableValue' && x.fields[0].value.type === 'VarargLiteral') {
        return `${lua_VarargLiteral_id}.clone()`;
    }
    if (x.fields.map(x => x.type === 'TableValue').reduce(((x, y) => x && y), true)) {
        const fs = x.fields;
        const xs = fs.map(x => do_table(x.value, scope));
        if (xs.length === 0) {
            return lua_vec("vec![]");
        }
        else {
            return lua_vec(`vec![${xs.reduce((x, y) => x + ", " + y)}]`);
        }
    }
    const map = [];
    let i = 1;
    const fs = x.fields;
    for (const item of fs) {
        assert(item.value.type !== 'VarargLiteral', '`{<other>, ...}` not supported');
        if (item.type === 'TableKey') {
            map.push([do_table(item.key, scope), do_table(item.value, scope)]);
        }
        else if (item.type === 'TableKeyString') {
            map.push([lua_str(item.key.name), do_table(item.value, scope)]);
        }
        else { // TableValue
            map.push([lua_num(i), do_table(item.value, scope)]); // 此处可能和Lua实现不同。
            i++;
        }
    }
    return lua_table(map);
}
function do_ForGenericStatement(x, scope) {
    if (x.iterators.length === 1 && x.iterators[0].type === 'CallExpression' && x.iterators[0].base.type === 'Identifier' && x.iterators[0].arguments.length === 1) {
        const argv = x.iterators[0].arguments[0];
        const s = scope_copy0(scope);
        let type = 'pairs';
        if (x.iterators[0].base.name === 'ipairs') {
            type = 'ipairs';
        }
        else if (x.iterators[0].base.name === 'pairs') {
            type = 'pairs';
        }
        else {
            throw 'ForGenericStatement: not ipairs/pairs';
        }
        if (x.variables.length === 2) {
            const v1 = x.variables[0].name;
            const v2 = x.variables[1].name;
            s.local.add(v1);
            s.local.add(v2);
            const body = do_table_map(x.body, s);
            if (type === 'ipairs') {
                return `for (_lua_tmp_k, _lua_tmp_v) in ${do_table(argv, scope)}.as_table(${lua_loc(argv)}).read().unwrap().ipairs() {\nlet ${v1}=${lua_make_var(`_lua_num!(_lua_tmp_k+1)`)};\nlet ${v2}=${lua_make_var(`_lua_tmp_v.clone()`)};\n${body}}\n`;
            }
            else {
                const _t = type;
                assert(_t === 'pairs');
                return `for (_lua_tmp_k, _lua_tmp_v) in ${do_table(argv, scope)}.as_table(${lua_loc(argv)}).read().unwrap().pairs() {\nlet ${v1}=${lua_make_var(`_lua_tmp_k.clone()`)};\nlet ${v2}=${lua_make_var(`_lua_tmp_v.clone()`)};\n${body}}\n`;
            }
        }
        else if (x.variables.length === 1) {
            const v1 = x.variables[0].name;
            s.local.add(v1);
            const body = do_table_map(x.body, s);
            if (type === 'ipairs') {
                return `for (_lua_tmp_k, _) in ${do_table(argv, scope)}.as_table(${lua_loc(argv)}).read().unwrap().ipairs() {\nlet ${v1}=${lua_make_var(`_lua_num!(_lua_tmp_k+1)`)};\n${body}}\n`;
            }
            else {
                const _t = type;
                assert(_t === 'pairs');
                return `for (_lua_tmp_k, _) in ${do_table(argv, scope)}.as_table(${lua_loc(argv)}).read().unwrap().pairs() {\nlet ${v1}=${lua_make_var(`_lua_tmp_k.clone()`)};\n${body}}\n`;
            }
        }
        else {
            throw 'ForGenericStatement: not `for k,v in` or `for k in`';
        }
    }
    else {
        throw 'ForGenericStatement: not `for ... in f(x) in`';
    }
}
function do_ForNumericStatement(x, scope) {
    throw 'WIP: ForNumericStatement';
}
function do_FunctionDeclaration(x, outter_scope) {
    const id = x.identifier;
    if (id === null) {
        const inner_scope = scope_new_closure();
        const arg_id = `_lua_arg_tmp`;
        let result = "";
        const args = x.parameters;
        for (let i = 0; i < args.length; i++) {
            const arg = args[i];
            if (arg.type === 'VarargLiteral') {
                result += `let mut ${lua_VarargLiteral_id} = ${lua_vec(arg_id)};\n`;
            }
            else { // Identifier
                inner_scope.local.add(arg.name);
                result += `let ${arg.name} = ${lua_make_var(`if ${arg_id}.is_empty() { ${lua_nil} } else { ${arg_id}.remove(0) }`)};\n`;
            }
        }
        result += do_table_map(x.body, inner_scope);
        for (const x of Array.from(inner_scope.used_nonlocal)) {
            scope_use(outter_scope, x);
        }
        return lua_lambda(arg_id, result, inner_scope);
    }
    else {
        if (id.type === 'MemberExpression') {
            if (id.indexer === ':') {
                throw '`function o:m() end` not implemented';
            }
            else {
                let _i = id.indexer;
                assert(_i === '.');
            }
        }
        const func = object_shadow_copy(x);
        func.identifier = null;
        if (x.isLocal) {
            // 为了支持递归。
            return do_table_map([
                { type: "LocalStatement",
                    variables: [id],
                    init: [],
                    loc: lua_get_loc(x) },
                { type: "AssignmentStatement",
                    variables: [id],
                    init: [func],
                    loc: lua_get_loc(x) },
            ], outter_scope);
        }
        else {
            return do_table({ type: "AssignmentStatement",
                variables: [id],
                init: [func],
                loc: lua_get_loc(x) }, outter_scope);
        }
    }
}
function do_LocalStatement(x, scope) {
    if (x.init.length === 0) {
        return x.variables.map(x => {
            scope.local.add(x.name);
            return `let ${x.name} = ${lua_make_var(lua_nil)};\n`;
        }).reduce((x, y) => x + y);
    }
    else {
        assert(x.init.length === 1 && x.variables.length === 1, `multiple values assign not supported`);
        scope.local.add(x.variables[0].name);
        return `let ${x.variables[0].name} = ${lua_make_var(do_table(x.init[0], scope))};\n`;
    }
}
function do_IfStatement(x, scope) {
    let result = "";
    const cs = x.clauses;
    for (const c of cs) {
        if (c.type === 'IfClause') {
            result += `if (${do_table(c.condition, scope)}).as_bool(${lua_loc(c.condition)}) {\n`;
            result += do_table_map(c.body, scope);
            result += `}`;
        }
        else if (c.type === 'ElseifClause') {
            result += ` else if (${do_table(c.condition, scope)}).as_bool(${lua_loc(c.condition)}) {\n`;
            result += do_table_map(c.body, scope);
            result += `}`;
        }
        else if (c.type === 'ElseClause') {
            result += ` else {\n`;
            result += do_table_map(c.body, scope);
            result += `}`;
        }
    }
    result += `\n`;
    return result;
}
function do_table_map(xs, scope) {
    return do_table({ type: "Chunk", body: xs }, scope);
}
function do_table(x, scope) {
    assert(x.type in table, `${x.type} not supported`);
    return table[x.type](x, scope);
}
const inner_compile = do_table;
exports.inner_compile = inner_compile;
