/*
    lua2rust - Prelude
    Copyright (C) 2019  Zaoqi <zaomir@outlook.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
#![allow(dead_code)]
#![allow(unused_macros)]
#![allow(unused_mut)]
#![allow(unused_variables)]
#![allow(non_snake_case)]
#![allow(non_camel_case_types)]
#![allow(unreachable_code)]
#![allow(unused_parens)]

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct _lua_debug_loc {
    // line + column
    pub start: (usize, usize),
    pub end: (usize, usize),
}
impl std::fmt::Display for _lua_debug_loc {
    #[inline]
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "( start: ( line: {}, column: {} ), end: ( line: {}, column: {} ) )", self.start.0, self.start.1, self.end.0, self.end.1)
    }
}
pub type _lua_data = std::sync::Arc<_lua_data_unpack>;
#[inline]
pub fn _lua_data__pack(data: _lua_data_unpack) -> _lua_data {
    std::sync::Arc::new(data)
}
pub struct _lua_data_unpack_function (Box<Fn(Vec<_lua_data>, _lua_debug_loc) -> _lua_data>);
impl std::hash::Hash for _lua_data_unpack_function {
    #[inline]
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        let sel: *const (Fn(Vec<_lua_data>, _lua_debug_loc) -> _lua_data) = &*self.0;
        sel.hash(state);
    }
}
impl PartialEq for _lua_data_unpack_function {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        let sel: *const (Fn(Vec<_lua_data>, _lua_debug_loc) -> _lua_data) = &*self.0;
        let other: *const (Fn(Vec<_lua_data>, _lua_debug_loc) -> _lua_data) = &*other.0;
        sel == other
    }
}
impl Eq for _lua_data_unpack_function {}
impl std::fmt::Debug for _lua_data_unpack_function {
    #[inline]
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let sel: *const (Fn(Vec<_lua_data>, _lua_debug_loc) -> _lua_data) = &*self.0;
        write!(f, "_lua_data_unpack_function({:p})", sel)
    }
}
#[derive(Debug)]
pub enum _lua_data_unpack_table {
    Map(std::collections::HashMap<_lua_data,_lua_data>),
    Vec(Vec<_lua_data>),
}
impl _lua_data_unpack_table {
    #[inline]
    pub fn pairs(&self) -> std::collections::hash_map::IntoIter<_lua_data,_lua_data> {
        match self {
            _lua_data_unpack_table::Map(v) => v.clone().into_iter(),
            _lua_data_unpack_table::Vec(v) => _lua_vec2table(v).into_iter(),
        }
    }
    #[inline]
    pub fn ipairs(&self) -> std::iter::Enumerate<std::slice::Iter<_lua_data>> {
        match self {
            _lua_data_unpack_table::Map(v) => panic!("WIP"),
            _lua_data_unpack_table::Vec(v) => v.iter().enumerate(),
        }
    }
}
impl std::hash::Hash for _lua_data_unpack_table {
    #[inline]
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        match self {
            _lua_data_unpack_table::Map(x) => {
                let sel: *const (std::collections::HashMap<_lua_data,_lua_data>) = &*x;
                sel.hash(state);
            },
            _lua_data_unpack_table::Vec(x) => {
                let sel: *const Vec<_lua_data> = &*x;
                sel.hash(state);
            },
        }
    }
}
impl PartialEq for _lua_data_unpack_table {
    fn eq(&self, other: &Self) -> bool {
        if let (_lua_data_unpack_table::Map(x), _lua_data_unpack_table::Map(y)) = (self, other) {
            let x: *const (std::collections::HashMap<_lua_data,_lua_data>) = &*x;
            let y: *const (std::collections::HashMap<_lua_data,_lua_data>) = &*y;
            x == y
        } else if let (_lua_data_unpack_table::Vec(x), _lua_data_unpack_table::Vec(y)) = (self, other) {
            let x: *const Vec<_lua_data> = &*x;
            let y: *const Vec<_lua_data> = &*y;
            x == y
        } else {
            false
        }
    }
}
impl Eq for _lua_data_unpack_table {}
#[derive(Debug)]
pub struct _lua_data_unpack_number (f64);
impl std::hash::Hash for _lua_data_unpack_number {
    #[inline]
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.0.to_bits().hash(state);
    }
}
impl PartialEq for _lua_data_unpack_number {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        self.0 == other.0 // 此处可能和Lua实现不同。
    }
}
impl Eq for _lua_data_unpack_number {}
#[derive(Debug)]
pub struct _lua_data_unpack_table_pack (std::sync::RwLock<_lua_data_unpack_table>);
impl std::hash::Hash for _lua_data_unpack_table_pack {
    #[inline]
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.0.read().unwrap().hash(state)
    }
}
impl PartialEq for _lua_data_unpack_table_pack {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        *self.0.read().unwrap() == *other.0.read().unwrap()
    }
}
impl Eq for _lua_data_unpack_table_pack {}
#[derive(Hash,PartialEq,Eq,Debug)]
pub enum _lua_data_unpack {
    Number(_lua_data_unpack_number),
    String(Vec<char>),
    Table(_lua_data_unpack_table_pack),
    Function(_lua_data_unpack_function),
    True,
    False,
    Nil,
}
impl std::fmt::Display for _lua_data_unpack {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            _lua_data_unpack::Number(_lua_data_unpack_number(x)) => write!(f, "{}", x),
            _lua_data_unpack::String(x) => write!(f, "{}", x.iter().collect::<String>()),
            _lua_data_unpack::Table(x) => {
                match &*x.0.read().unwrap() {
                    _lua_data_unpack_table::Map(v) => {
                        let p: *const (std::collections::HashMap<_lua_data,_lua_data>) = v;
                        write!(f, "table: {:p}", p)
                    },
                    _lua_data_unpack_table::Vec(v) => {
                        let p: *const Vec<_lua_data> = v;
                        write!(f, "table: {:p}", p)
                    },
                }},
            _lua_data_unpack::Function(x) => {
                let p: *const (Fn(Vec<_lua_data>, _lua_debug_loc) -> _lua_data) = &*x.0;
                write!(f, "function: {:p}", p)},
            _lua_data_unpack::True => write!(f, "true"),
            _lua_data_unpack::False => write!(f, "false"),
            _lua_data_unpack::Nil => write!(f, "nil"),
        }
    }
}
impl _lua_data_unpack {
    #[inline]
    pub fn as_bool(&self, loc: _lua_debug_loc) -> bool {
        match self {
            _lua_data_unpack::False | _lua_data_unpack::Nil => false,
            _ => true,
        }
    }
    #[inline]
    pub fn as_f64(&self, loc: _lua_debug_loc) -> f64 {
        if let _lua_data_unpack::Number(x) = self {
            x.0
        } else {
            panic!("isn't number: {:?}\nat {}", self, loc)
        }
    }
    #[inline]
    pub fn lua_tonumber(&self) -> _lua_data {
        if let _lua_data_unpack::String(x) = self {
            if let Ok(r) = x.iter().collect::<String>().parse::<f64>() {
                _lua_num(r)
            } else {
                _lua_nil()
            }
        } else if let _lua_data_unpack::Number(x) = self {
            _lua_data__pack(_lua_data_unpack::Number(_lua_data_unpack_number(x.0)))
        } else {
            _lua_nil()
        }
    }
    #[inline]
    pub fn as_string(&self, loc: _lua_debug_loc) -> String {
        if let _lua_data_unpack::String(x) = self {
            x.iter().collect()
        } else {
            panic!("isn't string: {:?}\nat {}", self, loc)
        }
    }
    #[inline]
    pub fn as_vecchar(&self, loc: _lua_debug_loc) -> &Vec<char> {
        if let _lua_data_unpack::String(x) = self {
            x
        } else {
            panic!("isn't string: {:?}\nat {}", self, loc)
        }
    }
    #[inline]
    pub fn from_bool(x: bool) -> Self {
        if x { _lua_data_unpack::True } else { _lua_data_unpack::False }
    }
    #[inline]
    pub fn as_table(&self, loc: _lua_debug_loc) -> &std::sync::RwLock<_lua_data_unpack_table> {
        if let _lua_data_unpack::Table(x) = self {
            &x.0
        } else {
            panic!("isn't table: {:?}\nat {}", self, loc)
        }
    }
}
#[inline]
pub fn _lua_str<'a>(x: &'a str) -> _lua_data {
    _lua_data__pack(_lua_data_unpack::String(x.chars().collect()))
}
#[macro_export]
macro_rules! _lua_num {
    ($x:expr) => (_lua_num($x as f64))
}
#[inline]
pub fn _lua_num(x: f64) -> _lua_data {
    _lua_data__pack(_lua_data_unpack::Number(_lua_data_unpack_number(x)))
}
#[inline]
pub fn _lua_table(xs: Vec<(_lua_data, _lua_data)>) -> _lua_data {
    _lua_data__pack(_lua_data_unpack::Table(_lua_data_unpack_table_pack(std::sync::RwLock::new(_lua_data_unpack_table::Map(xs.iter().cloned().collect())))))
}
#[inline]
pub fn _lua_len(xs: _lua_data, loc: _lua_debug_loc) -> _lua_data {
    if let _lua_data_unpack::Table(t) = &*xs {
        match &*t.0.read().unwrap() {
            _lua_data_unpack_table::Map(v) => _lua_num!(v.len()), // FIX ME 此处与Lua不同
            _lua_data_unpack_table::Vec(v) => _lua_num!(v.len()),
        }
    } else if let _lua_data_unpack::String(x) = &*xs {
        _lua_num!(x.len())
    } else {
        panic!("attempt to get length of a value that isn't a table: {:?}\nat: {}", xs, loc)
    }
}
pub fn _lua_lookup(map: _lua_data, key: _lua_data, loc: _lua_debug_loc) -> _lua_data {
    if let _lua_data_unpack::Table(t) = &*map {
        match &*t.0.read().unwrap() {
            _lua_data_unpack_table::Map(v) => {
                if let Some(x) = v.get(&key) {
                    x.clone()
                } else {
                    _lua_data__pack(_lua_data_unpack::Nil)
                }},
            _lua_data_unpack_table::Vec(xs) => {
                if let _lua_data_unpack::Number(_lua_data_unpack_number(key)) = *key {
                    let k = (key-1.0) as usize;
                    if k as f64 == (key-1.0) && k < xs.len() {
                        xs[k].clone()
                    } else {
                        _lua_data__pack(_lua_data_unpack::Nil)
                    }
                } else {
                    _lua_data__pack(_lua_data_unpack::Nil)
                }
            },
        }
    } else {
        panic!("attempt to index a value that isn't a table: {:?}\nat: {}", map, loc)
    }
}
pub fn _lua_set(map: _lua_data, key: _lua_data, val: _lua_data, loc: _lua_debug_loc) {
    if let _lua_data_unpack::Table(t) = &*map {
        let t: &mut _lua_data_unpack_table = &mut t.0.write().unwrap();
        match t {
            _lua_data_unpack_table::Map(v) => {v.insert(key, val);},
            _lua_data_unpack_table::Vec(xs) => match *key {
                _lua_data_unpack::Number(_lua_data_unpack_number(k))
                    if ((k-1.0) as usize) as f64 == (k-1.0) && ((k-1.0) as usize) <= xs.len() => {
                        let key = (k-1.0) as usize;
                        if (key == xs.len()) {
                            xs.push(val)
                        } else {
                            xs[key] = val
                        }
                    },
                _ => {
                    let mut v = _lua_vec2table(&xs);
                    v.insert(key, val);
                    *t = _lua_data_unpack_table::Map(v);
                }
            },
        }
    } else {
        panic!("attempt to index a value that isn't a table: {:?}\nat: {}", map, loc);
    }
}
#[inline]
pub fn _lua_vec(xs: Vec<_lua_data>) -> _lua_data {
    _lua_data__pack(_lua_data_unpack::Table(_lua_data_unpack_table_pack(std::sync::RwLock::new(_lua_data_unpack_table::Vec(xs)))))
}
#[inline]
pub fn _lua_vec2table(xs: &Vec<_lua_data>) -> std::collections::HashMap<_lua_data, _lua_data> {
    let mut result = std::collections::HashMap::new();
    for i in 1..=xs.len() {
        result.insert(_lua_num(i as f64), xs[i-1].clone());
    }
    result
}
#[inline]
pub fn _lua_lambda(f: Box<Fn(Vec<_lua_data>, _lua_debug_loc) -> _lua_data>) -> _lua_data {
    _lua_data__pack(_lua_data_unpack::Function(_lua_data_unpack_function(f)))
}
#[inline]
pub fn _lua_call(f: _lua_data, args: Vec<_lua_data>, loc: _lua_debug_loc) -> _lua_data {
    if let _lua_data_unpack::Function(v) = &*f {
        v.0(args, loc)
    } else {
        panic!("attempt to call a value that isn't a function: {:?}\nat: {}", f, loc)
    }
}
#[inline]
pub fn _lua_not(x: _lua_data, loc: _lua_debug_loc) -> _lua_data {
    _lua_bool(!x.as_bool(loc))
}
#[inline]
pub fn _lua_bool(x: bool) -> _lua_data {
    _lua_data__pack(_lua_data_unpack::from_bool(x))
}
#[macro_export]
macro_rules! _lua_op {
    (or, $x: expr, $y: expr, $loc: expr) => (if $x.as_bool($loc) { $x } else { $y });
    (and, $x: expr, $y: expr, $loc: expr) => (if !$x.as_bool($loc) { $x } else { $y });
    (add, $x: expr, $y: expr, $loc: expr) => (_lua_num($x.as_f64($loc) + $y.as_f64($loc)));
    (sub, $x: expr, $y: expr, $loc: expr) => (_lua_num($x.as_f64($loc) - $y.as_f64($loc)));
    (mul, $x: expr, $y: expr, $loc: expr) => (_lua_num($x.as_f64($loc) * $y.as_f64($loc)));
    (div, $x: expr, $y: expr, $loc: expr) => (_lua_num($x.as_f64($loc) / $y.as_f64($loc)));
    (rem, $x: expr, $y: expr, $loc: expr) => (panic!("not implemented: rem\nat: {}", $loc));
    (exp, $x: expr, $y: expr, $loc: expr) => (panic!("not implemented: exp\nat: {}", $loc));
    (eq, $x: expr, $y: expr, $loc: expr) => (_lua_bool($x == $y));
    (not_eq, $x: expr, $y: expr, $loc: expr) => (_lua_bool($x != $y));
    (less_eq, $x: expr, $y: expr, $loc: expr) => (_lua_bool($x.as_f64($loc) <= $y.as_f64($loc)));
    (greater_eq, $x: expr, $y: expr, $loc: expr) => (_lua_bool($x.as_f64($loc) >= $y.as_f64($loc)));
    (less, $x: expr, $y: expr, $loc: expr) => (_lua_bool($x.as_f64($loc) < $y.as_f64($loc)));
    (greater, $x: expr, $y: expr, $loc: expr) => (_lua_bool($x.as_f64($loc) > $y.as_f64($loc)));
    // https://stackoverflow.com/questions/40792801/best-way-to-concatenate-vectors-in-rust
    (concat, $x: expr, $y: expr, $loc: expr) => (_lua_data__pack(_lua_data_unpack::String($x.as_vecchar($loc).iter().cloned().chain($y.as_vecchar($loc).iter().cloned()).collect())));
}
#[inline]
pub fn _lua_nil() -> _lua_data {
    _lua_data__pack(_lua_data_unpack::Nil)
}
#[inline]
pub fn _lua_true() -> _lua_data {
    _lua_data__pack(_lua_data_unpack::True)
}
#[inline]
pub fn _lua_false() -> _lua_data {
    _lua_data__pack(_lua_data_unpack::False)
}
#[inline]
pub fn _lua_neg(x: _lua_data, loc: _lua_debug_loc) -> _lua_data {
    _lua_num(-x.as_f64(loc))
}
pub fn template() -> _lua_data {
    // https://www.lua.org/manual/5.3/manual.html#pdf-print
    let print = std::sync::Arc::new(std::sync::RwLock::new(_lua_lambda(Box::new(|xs, _| {
        println!("{}", xs.iter().fold(String::from(""), |acc, x| if acc == "" { format!("{}", x) } else { acc + "\t" + &format!("{}", x) }));
        _lua_nil()
    }))));
    // https://www.lua.org/manual/5.3/manual.html#pdf-tostring
    let tostring = std::sync::Arc::new(std::sync::RwLock::new(_lua_lambda(Box::new(|mut xs, _| {
        let x = if xs.is_empty() { _lua_nil() } else { xs.remove(0) };
        _lua_str(&format!("{}", x))
    }))));
    // https://www.lua.org/manual/5.3/manual.html#pdf-tonumber
    // base未实现
    let tonumber = std::sync::Arc::new(std::sync::RwLock::new(_lua_lambda(Box::new(|mut xs, loc| {
        let arg: _lua_data = if xs.is_empty() { _lua_nil() } else { xs.remove(0) };
        arg.lua_tonumber()
    }))));
    // https://www.lua.org/manual/5.3/manual.html#pdf-error
    // level未实现
    let error = std::sync::Arc::new(std::sync::RwLock::new(_lua_lambda(Box::new(|mut xs, loc| {
        let message: _lua_data = if xs.is_empty() { _lua_nil() } else { xs.remove(0) };
        panic!("{}\nat: {}", message.as_string(loc), loc)
    }))));
    let table = std::sync::Arc::new(std::sync::RwLock::new(_lua_table(vec![
        // https://www.lua.org/manual/5.3/manual.html#pdf-table.insert
        (_lua_str("insert"), _lua_lambda(Box::new(|mut xs, loc| {
            if xs.len() <= 2 {
                let list = if xs.is_empty() { _lua_nil() } else { xs.remove(0) };
                let value = if xs.is_empty() { _lua_nil() } else { xs.remove(0) };
                if let _lua_data_unpack_table::Vec(v) = &mut *list.as_table(loc).write().unwrap() {
                    v.push(value);
                } else {
                    panic!("WIP");
                };
            } else {
                let list = if xs.is_empty() { _lua_nil() } else { xs.remove(0) };
                let pos = ((if xs.is_empty() { _lua_nil() } else { xs.remove(0) }).as_f64(loc) - 1.0) as usize;
                let value = if xs.is_empty() { _lua_nil() } else { xs.remove(0) };
                if let _lua_data_unpack_table::Vec(v) = &mut *list.as_table(loc).write().unwrap() {
                    v.insert(pos, value);
                } else {
                    panic!("WIP");
                };
            }
            _lua_nil()
        }))),
        // https://www.lua.org/manual/5.3/manual.html#pdf-table.remove
        (_lua_str("remove"), _lua_lambda(Box::new(|mut xs, loc| {
            let mut result;
            if xs.len() <= 1 {
                let list = if xs.is_empty() { _lua_nil() } else { xs.remove(0) };
                if let _lua_data_unpack_table::Vec(v) = &mut *list.as_table(loc).write().unwrap() {
                    result = v.pop().unwrap();
                } else {
                    panic!("WIP");
                };
            } else {
                let list = if xs.is_empty() { _lua_nil() } else { xs.remove(0) };
                let pos = ((if xs.is_empty() { _lua_nil() } else { xs.remove(0) }).as_f64(loc) - 1.0) as usize;
                if let _lua_data_unpack_table::Vec(v) = &mut *list.as_table(loc).write().unwrap() {
                    result = v.remove(pos);
                } else {
                    panic!("WIP");
                };
            }
            result
        }))),
    ])));
    let string = std::sync::Arc::new(std::sync::RwLock::new(_lua_table(vec![
        // https://www.lua.org/manual/5.3/manual.html#pdf-string.sub
        // 只支持正整数
        (_lua_str("sub"), _lua_lambda(Box::new(|mut xs, loc| {
            let s = (if xs.is_empty() { _lua_nil() } else { xs.remove(0) });
            let i = (if xs.is_empty() { _lua_nil() } else { xs.remove(0) }).as_f64(loc) as usize;
            let j = (if xs.is_empty() { _lua_nil() } else { xs.remove(0) }).as_f64(loc) as usize;
            _lua_data__pack(_lua_data_unpack::String(s.as_vecchar(loc)[i-1..j].to_vec()))
        }))),
    ])));
    // Test Block [[[
    // ]]] Test Block
    _lua_nil()
}
