print(print==print)
local a=function()end
local b=function()end
print(a==a)
print(a~=b)
local function c()
return 0
end
print(c())

local i=0
local function addi()
local f
f=function(j)
i=i+j
end
f(1)
end
print(i==0)
addi()
print(i==1)
addi()
print(i==2)


local t=(function (...) return {...} end)(1,2,3)
print(t[3]==3 and t[2]==2 and t[1]==1 and #t==3)
