#!/bin/sh
set -e
fail(){
    local name="$1"
    local output="$2"
    local expect="$3"
    echo "!!!Test Failed" >&2
    echo "!!!name: $name" >&2
    echo "!!!expect" >&2
    echo "$expect" >&2
    echo "!!!output" >&2
    echo "$output" >&2
    exit 1
}
for f in *.lua;do
    name="${f%.lua}"
    ../bin/lua2rust.js "$f" "$name" > "$name.rs"
    echo "fn main() { "$name"(); }" >> "$name.rs"
    rustc -o "$name.out" "$name.rs"
    output="$("./$name.out")"
    expect="$(cat "$name.output")"
    [ "$output" = "$expect" ] || fail "$name" "$output" "$expect"
done
