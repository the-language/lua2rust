#!/usr/bin/env node

const lua2rust=require('../lua2rust.js')
const fs=require('fs')
const args=require('yargs')
      .usage('Usage: $0 {lua file} {output function name}')
      .help('h').alias('h', 'help')
      .example('$0 a.lua a','Transpile a.lua to Rust Function `a`')
      .demandCommand(2, 'Invalid options, specify a file and a function name')
      .argv._
const file=args[0]
const name=args[1]
const code=fs.readFileSync(file,'utf-8')
const template=fs.readFileSync(__dirname+'/../output_template.rs', 'utf-8').split('\n')

let template_head=[]
let template_init_head=[]
let template_init_tail=[]
while(template[0]!=='pub fn template() -> _lua_data {'){
    template_head.push(template.shift())
}
template.shift()
while(template[0]!=='    // Test Block [[['){
    template_init_head.push(template.shift())
}
template.shift()
while(template[0]!=='    // ]]] Test Block'){
    template.shift()
}
template.shift()
while(template[0]!=='}'){
    template_init_tail.push(template.shift())
}

console.log(
    template_head.reduce((x,y)=>x+"\n"+y)+"\n"+
    `pub fn ${name}() -> _lua_data {\n`+
    template_init_head.reduce((x,y)=>x+"\n"+y)+"\n"+
    lua2rust.inner_compile(lua2rust.pre(code),lua2rust.scope_new_closure())+
    template_init_tail.reduce((x,y)=>x+"\n"+y)+"\n"+
    '}'
)
