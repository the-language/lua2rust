# lua2rust

[![npm package](https://img.shields.io/npm/v/lua2rust.svg)](https://www.npmjs.org/package/lua2rust)
[![npm downloads](http://img.shields.io/npm/dm/lua2rust.svg)](https://www.npmjs.org/package/lua2rust)

A Lua to Rust transpiler.

Usage: `npx lua2rust {lua file} {output function name} > {output}`

Examples: [example](example)

## Different from Lua5.*

* no `loadstring`, metatable, user-defined iterator ...
* `table.insert` `#t` `for k,v in ipairs(t)` ... Works correctly only on consecutive arrays starting at 1.
* Behavior when modify the iterated object when iterating
* `tonumber (e [, base])`: `base` not implemented
* `error (message [, level])`: `level` not implemented
* `string.sub` can only receive one string and two positive integers.
* `string.sub`, `#<a string>`, ... use [`char`, `.chars()`](https://doc.rust-lang.org/stable/std/string/struct.String.html#method.chars), not `u8`
* `<a string>[...]` (Lua5.* : `nil`) (lua2rust : panic)
* ...
